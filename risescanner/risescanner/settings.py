from base_settings import *

ROOT_URLCONF = 'risescanner.urls'

ALLOWED_HOSTS.append('127.0.0.1')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'risescanner',
        'USER': 'rise.scanner',
        'PASSWORD': 'AllAboutTheSecretP@$$w0rd',
        'HOST': 'localhost',
        'PORT': '',
    }
}

INSTALLED_APPS.extend([ 'apps.riseblockchain', 'silk'] )

MIDDLEWARE.append('silk.middleware.SilkyMiddleware')

