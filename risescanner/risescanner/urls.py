"""risescanner URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from apps.riseblockchain.views import index as bc_index, gettxlist, getdglist, getbklist, account, transaction
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', bc_index),
    url(r'^gettxlist/(?P<order>\w+)/(?P<asc>\w+)/(?P<page>\d+)/$', gettxlist),
    url(r'^getdglist/(?P<order>\w+)/(?P<asc>\w+)/(?P<page>\d+)/$', getdglist),
    url(r'^getbklist/(?P<order>\w+)/(?P<asc>\w+)/(?P<page>\d+)/$', getbklist),
    url(r'^gettxslist/(?P<sid>[R0-9]+)/(?P<order>\w+)/(?P<asc>\w+)/(?P<page>\d+)/$', gettxlist),
    url(r'^gettxrlist/(?P<rid>[R0-9]+)/(?P<order>\w+)/(?P<asc>\w+)/(?P<page>\d+)/$', gettxlist),    
    url(r'^acnt/(?P<id>[R0-9]+)/$', account),
    url(r'^tx/(?P<id>\d+)/$', transaction),
    
]


urlpatterns += [url(r'^silk/', include('silk.urls', namespace='silk'))]