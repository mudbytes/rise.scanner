from django.shortcuts import render, redirect
from django.http import Http404
from rise import RiseAPI

form apps.riseblockchain.forms. import SearchForm

# Utility Functions to keep it DRY

def connect_rise()
    return RiseAPI('http://127.0.0.1:5555')

def get_list(api, order, asc, page, orderBy, default, extras=None):
    try:
        order = "%s:%s" % (orderBy[order], asc)
    except KeyError:
        order = default
    try:
        count = int(api(limit=1)['totalCount'])
    except KeyError:
        try:
            count = int(api(limit=1)['count'])
        except KeyError:
            count = api(limit=1)['blocks'][0]['height']
            
    if page <= 0:
        page = 1
    if page * 10 > count:
        page = count / 10
    offset = (page-1)*10 
    query = {'offset':offset, 'orderBy':order, 'limit':10}
    if extras:
        query.update(extras)
    
    return api(**query)

def get_transaction_or_404(id, api):
    return or_404(api.transactions.get, id)

def get_account_or_404(id, api):
    return or_404(api.accounts.get_account, id)
    
def get_block_or_404(id, api):
    return or_404(api.blocks.get_block, id)

def or_404(method, id):
    result = method(id)
    if not result['success']:
        raise Http404
    return result['account']
    

# Rise Scanner Views.

def index(request):
    api = connect_rise()
    context = {}
    context['block_height'] = api.blocks.get_height()['height']
    context['version'] = api.peers.version()['version']

    tx = get_transactions(request, api, 'timestamp', 'desc', page=1)
    context['last_txpage'] = int(int(tx['count']) / 10)
    context['transactions'] = tx['transactions']
    
    dg = get_delegates(request, api, 'rank', 'asc', page=1)
    context['last_dgpage'] = int(int(dg['totalCount']) / 10)
    context['delegates'] = dg['delegates']
     
    bk = get_blocks(request, api, 'height', 'desc', page=1)
    context['last_bkpage'] = int(int(api.blocks.get_height()['height']) / 10)
    context['blocks'] = bk['blocks']
    
    return render(request, "index.html", context)

def get_transactions(request, api, order, asc='asc', page=1, extras):
    page = int(page)
    orderBy = {
               'to': 'recipientId',
               'from': 'senderId',
               'type': 'type',
               'time': 'timestamp',
               'amount': 'amount',
              }
    return get_list(api.transactions.get_list, order, asc, page, orderBy, 'timestamp:desc', extras)


def get_delegates(request, api, order, asc='asc', page=1):
    page = int(page)
    orderBy = {
               'rank': 'rank',
               'dg': 'address',
               'vote': 'vote',
               'product': 'productivity',
               'produce': 'producedblocks', #This doesn't work but hopefully they change it.
               'approval': 'approval',
              }
    return get_list(api.delegates.get_list, order, asc, page, orderBy, 'rank:asc')

def get_blocks(request, api, order, asc='asc', page=1):
    page = int(page)
    orderBy = {
               'fee': 'totalFee',
               'reward': 'reward',
               'generator': 'generatorPublicKey',
               'height': 'height',
               'time': 'timestamp',
               'volume': 'totalAmount',
               'transactions': 'numberOfTransactions',
               'id': 'id',
              }
    return get_list(api.blocks.get_blocks, order, asc, page, orderBy, 'height:desc')

def getdglist(request, order, asc, page):
    api = connect_rise()
    dglist = get_delegates(request, api, order, asc, page)
    
    return render(request, 'delegates_row.html', {'delegates': dglist['delegates']})

def gettxlist(request, order, asc, page, sid=None, rid=None):
    api = connect_rise()
    context = {}    
    if sid:
        txlist = get_transactions(request, api, order, asc, page, {'senderId': sid})
        context['sid'] = sid
    elif rid:
        txlist = get_transactions(request, api, order, asc, page, {'recipientId': rid}))
        context['rid'] = rid
    else:
        txlist = get_transactions(request, api, order, asc, page)
    
    context['transactions'] = txlist['transactions']
    return render(request, 'transaction_row.html', context)

def getbklist(request, order, asc, page):
    api = connect_rise()
    bklist = get_blocks(request, api, order, asc, page)
    
    return render(request, 'block_row.html', {'blocks': bklist['blocks']})

def account(request, id):
    api = connect_rise()
    context = {}
    limit = 10
    orderBy = 'timestamp:desc'
    offset = 0
    context['acnt'] = get_account_or_404(id, api)
    
    rres = api.transactions.get_list(limit=limit, orderBy=orderBy, offset=offset, recipientId=context['acnt']['address'])
    sres = api.transactions.get_list(limit=limit, orderBy=orderBy, offset=offset, senderId=context['acnt']['address'])
    
    context['txr_lastpage'] = int( int(rres['count']) / limit )
    context['txs_lastpage'] = int( int(sres['count']) / limit )
    context['rtransactions'] = rres['transactions']
    context['stransactions'] = sres['transactions']
    
    return render(request, 'account.html', context)

def block(request, id):
    api = connect_rise()
    
    context{}
    context['block'] = get_block_or_404(id)
    context['transactions'] = get_transactions(request, api, limit=10, orderBy='timestamp', asc='desc', 1, 
    
    

def transaction(request, id):
    api = connect_rise()
    context = {}
    
    context['t'] = get_transaction_or_404(id, api)

    if 'votes' in context['t']:
        r = api.delegates.get_by_public_key(context['t']['votes']['added'])
        if r['success'] == True:
            context['added'] = r['delegate']
        r = api.delegates.get_by_public_key(context['t']['votes']['deleted'])
        if r['success'] == True:
            context['deleted'] = r['delegate']
    return render(request, 'transaction.html', context)

def transactions(request, 'timestamp', 'desc', 1, limit=20):
    api = connect_rise()
    context = {}
    orderBy = "%s:%s" % (order, asc)
    limit = min(50, limit)
    last_page = int(api.transactions.get_list(limit=1)['count']) / limit
    page = max(1, min(last_page, page))
    offset = page * limit
    
    context['last_page'] = last_page
    context['page'] = page
    context['transactions'] = api.transactions.get_list(limit=limit, orderBy=orderBy, offset=offset)
    
    return render(request, 'transaction.html', context)

def search_blockchain(request):
    api = connect_rise()
    context = {}
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            search_methods = [(api.transaction.get, transaction),                              
                              (api.blocks.get_block, None),
                              (api.accounts.get_account, account),1
                             ]
            s = form.cleaned_data['search']
            for method in search_methods:
                r = method[0](s)
                if r.['success'] == True:
                    return method[1](request, s)

    else:
        form = SearchForm()
    return index(request)
