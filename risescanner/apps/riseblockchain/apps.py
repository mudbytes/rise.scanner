from django.apps import AppConfig


class RiseblockchainConfig(AppConfig):
    name = 'riseblockchain'
