import datetime
from django import template
from rise import RiseAPI
from django.http import HttpResponse

register = template.Library()
api = RiseAPI('http://127.0.0.1:5555')

@register.filter('strftime')
def template_strftime(timestamp):
    if not timestamp:
        timestamp = 0
    timestamp = int(timestamp) + 1464109200
    return datetime.datetime.fromtimestamp(int(timestamp))

@register.filter('coinify')
def coinify(coin):
    if not coin:
        coin = 0
    coin = int(coin)
    return coin / 100000000


@register.filter('linkTx')
def linkTx(id):
    
    result = api.transactions.get(id)
    if result['success'] == False:
        return id
    return '<a href="/tx/%s/">%s</a>' % (id, id)
    
@register.filter('linkAcnt')
def linkAcnt(id):
    result = api.accounts.get_account(id)
    
    if result['success'] == False:
        return id
    
    account = result['account']
    if id == '1644223775232371040R':
        return '<i class="fa fa-exchange" aria-hidden="true"></i> <a href="/acnt/%s/">Bittrex</a>' % id
    delres = api.delegates.get_by_public_key(account['publicKey'])
    if delres['success'] == True and len(delres['delegate']['username']) > 1:
        account['username'] = delres['delegate']['username']
        account['rank'] = delres['delegate']['rank']
        if account['rank'] <= 101:
            return '<i class="fa fa-diamond" aria-hidden="true"></i> <a href="/acnt/%s/">%s</a>' % (id, account['username'])
        return '<a href="/acnt/%s/">%s</a>' % (id, account['username'])
        
    return '<a href="/acnt/%s/">%s</a>' % (id, id)
    