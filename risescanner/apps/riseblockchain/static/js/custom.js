		jQuery(function() {
			//tabs
			jQuery('#tab-btn1').click(function(e) {
				jQuery('#tab1').fadeIn(500);
				jQuery('#tab2').fadeOut(500);
				jQuery('#tab3').fadeOut(500);
				jQuery('#tab4').fadeOut(500);
				jQuery('#tab5').fadeOut(500);
				jQuery('.services-sec .tab1 span i').addClass('open');
				jQuery('.services-sec .tab2 span i, .services-sec .tab3 span i, .services-sec .tab4 span i, .services-sec .tab5 span i').removeClass('open');
			});
			jQuery('#tab-btn2').click(function(e) {
				jQuery('#tab1').fadeOut(500);
				jQuery('#tab2').fadeIn(500);
				jQuery('#tab3').fadeOut(500);
				jQuery('#tab4').fadeOut(500);
				jQuery('#tab5').fadeOut(500);
				jQuery('.services-sec .tab2 span i').addClass('open');
				jQuery('.services-sec .tab1 span i, .services-sec .tab3 span i, .services-sec .tab4 span i, .services-sec .tab5 span i').removeClass('open');
			});
			jQuery('#tab-btn3').click(function(e) {
				jQuery('#tab1').fadeOut(500);
				jQuery('#tab2').fadeOut(100);
				jQuery('#tab3').fadeIn(500);
				jQuery('#tab4').fadeOut(500);
				jQuery('#tab5').fadeOut(500);
				jQuery('.services-sec .tab3 span i').addClass('open');
				jQuery('.services-sec .tab1 span i, .services-sec .tab2 span i, .services-sec .tab4 span i, .services-sec .tab5 span i').removeClass('open');
			});
			jQuery('#tab-btn4').click(function(e) {
				jQuery('#tab1').fadeOut(500);
				jQuery('#tab2').fadeOut(500);
				jQuery('#tab3').fadeOut(500);
				jQuery('#tab4').fadeIn(500);
				jQuery('#tab5').fadeOut(500);
				jQuery('.services-sec .tab4 span i').addClass('open');
				jQuery('.services-sec .tab1 span i, .services-sec .tab2 span i, .services-sec .tab3 span i, .services-sec .tab5 span i').removeClass('open');
			});
			jQuery('#tab-btn5').click(function(e) {
				jQuery('#tab1').fadeOut(500);
				jQuery('#tab2').fadeOut(500);
				jQuery('#tab3').fadeOut(500);
				jQuery('#tab4').fadeOut(500);
				jQuery('#tab5').fadeIn(500);
				jQuery('.services-sec .tab5 span i').addClass('open');
				jQuery('.services-sec .tab1 span i, .services-sec .tab2 span i, .services-sec .tab3 span i, .services-sec .tab4 span i').removeClass('open');
			});
			
			
			//add active class
			jQuery('.tabs-btn li a').click(function(e) {
				jQuery('.tabs-btn li a').removeClass('active');
				jQuery(this).addClass('active');
			});
			
			
			
			//top bar colors
			jQuery('.top-bar-colors li a#orange').click(function(e) {
				jQuery('.header-top').removeClass('green red blue purple');
				jQuery('.header-top').addClass('orange');
			});
			jQuery('.top-bar-colors li a#red').click(function(e) {
				jQuery('.header-top').removeClass('green orange blue purple');
				jQuery('.header-top').addClass('red');
			});
			jQuery('.top-bar-colors li a#green').click(function(e) {
				jQuery('.header-top').removeClass('orange red blue purple');
				jQuery('.header-top').addClass('green');
			});
			jQuery('.top-bar-colors li a#blue').click(function(e) {
				jQuery('.header-top').removeClass('green red orange purple');
				jQuery('.header-top').addClass('blue');
			});
			jQuery('.top-bar-colors li a#purple').click(function(e) {
				jQuery('.header-top').removeClass('green red blue orange');
				jQuery('.header-top').addClass('purple');
			});
			jQuery('.top-bar-colors li a#gray').click(function(e) {
				jQuery('.header-top').removeClass('green red blue orange purple');
			});
			
			//add active class in top bar
			jQuery('.top-bar-colors li a, .footer-colors li a').click(function(e) {
				jQuery('.top-bar-colors li a, .footer-colors li a').removeClass('active');
				jQuery(this).addClass('active');
			});
			
			
			//footer colors
			jQuery('.footer-colors li a#orange2').click(function(e) {
				jQuery('.footer-large').removeClass('green red blue purple');
				jQuery('.footer-large').addClass('orange');
			});
			jQuery('.footer-colors li a#red2').click(function(e) {
				jQuery('.footer-large').removeClass('green orange blue purple');
				jQuery('.footer-large').addClass('red');
			});
			jQuery('.footer-colors li a#green2').click(function(e) {
				jQuery('.footer-large').removeClass('orange red blue purple');
				jQuery('.footer-large').addClass('green');
			});
			jQuery('.footer-colors li a#blue2').click(function(e) {
				jQuery('.footer-large').removeClass('green red orange purple');
				jQuery('.footer-large').addClass('blue');
			});
			jQuery('.footer-colors li a#purple2').click(function(e) {
				jQuery('.footer-large').removeClass('green red blue orange');
				jQuery('.footer-large').addClass('purple');
			});
			jQuery('.footer-colors li a#gray2').click(function(e) {
				jQuery('.footer-large').removeClass('green red blue orange purple');
			});
			
			
			//add active class in footer
			jQuery('.footer-colors li a').click(function(e) {
				jQuery('.footer-colors li a').removeClass('active');
				jQuery(this).addClass('active');
			});
			
			//------------------------------------ EXPERIENCE -----------------------------------//
			
			jQuery('.experience .row .experience-content, .experience .row .devices').appear();
			jQuery(document.body).on('appear', '.experience .row .experience-content, .experience .row .devices', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			//------------------------------------ SERVICES -----------------------------------//
			
			jQuery('.services-sec .row').appear();
			jQuery(document.body).on('appear', '.services-sec .row', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).children('*').each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			
			//------------------------------------ TRI SECS -----------------------------------//
			
			jQuery('.tri-secs .col-md-6').appear();
			jQuery(document.body).on('appear', '.tri-secs .col-md-6', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			
			//------------------------------------ TEAM -----------------------------------//
			
			jQuery('.team .row .col-md-4').appear();
			jQuery(document.body).on('appear', '.team .row .col-md-4', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			//------------------------------------ TABLE -----------------------------------//
			
			jQuery('.tables .row .col-md-3').appear();
			jQuery(document.body).on('appear', '.tables .row .col-md-3', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			
			//------------------------------------ PORTFOLIO -----------------------------------//
			
			jQuery('#portfolio-items-wrap').appear();
			jQuery(document.body).on('appear', '#portfolio-items-wrap', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			//------------------------------------ SHOP -----------------------------------//
			
			jQuery('.best-sellers .row .col-md-4').appear();
			jQuery(document.body).on('appear', '.best-sellers .row .col-md-4', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			//------------------------------------ OUR CLIENTS -----------------------------------//
			
			jQuery('.our-clients .flipInY').appear();
			jQuery(document.body).on('appear', '.our-clients .flipInY', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			
			//------------------------------------ CONTACT US -----------------------------------//
			
			jQuery('.contact-us .form').appear();
			jQuery(document.body).on('appear', '.contact-us .form', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			//------------------------------------ SOCIAL -----------------------------------//
			
			jQuery('.social li').appear();
			jQuery(document.body).on('appear', '.social li', function(e, $affected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
				
	
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
			
			
			
			
			
			//------------------------------------ SWITCHER -----------------------------------//
			
			//jQuery('#dark').click(function(e) {
//				jQuery(this).toggleClass('active');
//				if($('#dark').hasClass('active'))
//					$('#dark-css').attr("href", "css/dark_theme.css"),
//					$('.parallax-main-banner').css("background-position", "0 0");
//				else
//					$('#dark-css').attr("href", "");
//				});
			
			
			
			
		})
        
var arrow_up = '<i class="fa fa-arrow-up" aria-hidden="true"></i>'
var arrow_down = '<i class="fa fa-arrow-down" aria-hidden="true"></i>'


$(function() {
    $(document).on('click', '.wait_page', function(e) {
        e.stopPropagation();
        e.preventDefault();        
        return false
    });
    
    $(document).on('click', '.next_page', function(e)
    {   var type = $(this).data('type')
        e.stopPropagation();
        e.preventDefault();
        $(this).toggleClass('next_page wait_page')
        getNextPage(type, $(this))
        
        return false
    });
    $(document).on('click', '.prev_page', function(e)
    {   var type = $(this).data('type')
        e.stopPropagation();
        e.preventDefault();        
        getPrevPage(type, $(this))
        return false
    });

    $(document).on('change', '.set_page', function()
    {   var page = $(this).val()
        var type = $(this).data('type')
        getPage(page, type)
    });
    
    $(document).on('click', '.reorder', function(e)
    {   var order = $(this).data('order')
        var active = $(this).data('active')
        var type = $(this).data('type')        
        var page = $(type + '_page').val()
        var arrow = 'up'
        
        $('.reorder').data('active', 'false')
        if( active == 'false' )
        {   active = 'desc'
            arrow = arrow_down
        }
        else if (active == 'desc')
        {   active = 'asc'
            arrow = arrow_up
        }
        else
        {   active = 'desc'
            arrow = arrow_down
        }
        
        $(this).data('active', active)
        $('.arrow').hide()
        $( $(this).data('arrow') ).show()
        $( $(this).data('arrow') ).html(arrow)
        $(type + '_orderBy').val(order + ':' + active)
        var base = typeToUrl(type, $(this))
        var url = base + order + '/' + active + '/' + page + '/'

        $.ajax({ // create an AJAX call...
                    data: $(this).serialize(), // get the form data
                    type: $(this).attr('method'), // GET or POST
                    url: url, // the file to call
                    success: function(response) { // on success..
                        $(type + '_tdbody').html(response); // update the DIV
                    }
                });            
        e.stopPropagation();
        e.preventDefault();                
        return false
           
        
    });

        
    
});

function typeToUrl(type, target=null)
{   if (type == '#bk')
        return '/getbklist/'
    if (type == '#tx')
        return '/gettxlist/'
    if (type == '#dg')
        return '/getdglist/'
    if (type == '#txs')
        return '/gettxslist/' + target.data('addr') + '/'
    if (type == '#txr')
        return '/gettxrlist/' + target.data('addr') + '/'        

}

function getPrevPage(type, target)
{   var page = Number($(type+ '_page').val()) - 1
    if(page <= 0) page = 1

    getPage(page, type, target)
}

function getNextPage(type, target)
{   var page = Number($(type+ '_page').val()) + 1
    if(page > $(type+ '_lastpage').val()) page = $(type+ '_lastpage').val()
    getPage(page, type, target)
}

function getPage(page, type, target)
{   var orderBy = $(type+ '_orderBy').val().split(':')
    var order = orderBy[0]
    var asc = orderBy[1]
    

    if(page <= 0)
        page = 1
    if( page > $(type+ '_lastpage').val() )
        page = $(type+ '_lastpage').val()

    var base = typeToUrl(type, target)
    var url = base + order + '/' + asc + '/' + page + '/'
    $.ajax({ // create an AJAX call...
                    data: $(this).serialize(), // get the form data
                    type: $(this).attr('method'), // GET or POST
                    url: url, // the file to call
                    success: function(response) { // on success..
                        $(type+ '_tdbody').html(response); // update the DIV
                        target.toggleClass('next_page wait_page')
                        $(type+ '_page').val(page)

                    }
                });            
}    
